<?php

namespace App\Entity;

use App\Repository\PartenaireRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PartenaireRepository::class)
 */
class Partenaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_entreprise;

    /**
     * @Assert\Email(message="The email'{{value}}' is not a valid email.")
     * @ORM\Column(type="string", length=255)
     */
    private $adresse_email_entreprise;

    /**@Assert\Email(message="The email'{{value}}' is not a valid email.")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse_email_suzosky_entreprise;

    /**
     * @Assert\Regex("/^[0-9]{16}$/")
     * @ORM\Column(type="string", length=255)
     */
    private $phone_entreprise;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_suzosky_entreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse_postale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse_geographique;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEntreprise(): ?string
    {
        return $this->nom_entreprise;
    }

    public function setNomEntreprise(string $nom_entreprise): self
    {
        $this->nom_entreprise = $nom_entreprise;

        return $this;
    }

    public function getAdresseEmailEntreprise(): ?string
    {
        return $this->adresse_email_entreprise;
    }

    public function setAdresseEmailEntreprise(string $adresse_email_entreprise): self
    {
        $this->adresse_email_entreprise = $adresse_email_entreprise;

        return $this;
    }

    public function getAdresseEmailSuzoskyEntreprise(): ?string
    {
        return $this->adresse_email_suzosky_entreprise;
    }

    public function setAdresseEmailSuzoskyEntreprise(?string $adresse_email_suzosky_entreprise): self
    {
        $this->adresse_email_suzosky_entreprise = $adresse_email_suzosky_entreprise;

        return $this;
    }

    public function getPhoneEntreprise(): ?string
    {
        return $this->phone_entreprise;
    }

    public function setPhoneEntreprise(string $phone_entreprise): self
    {
        $this->phone_entreprise = $phone_entreprise;

        return $this;
    }

    public function getPhoneSuzoskyEntreprise(): ?string
    {
        return $this->phone_suzosky_entreprise;
    }

    public function setPhoneSuzoskyEntreprise(?string $phone_suzosky_entreprise): self
    {
        $this->phone_suzosky_entreprise = $phone_suzosky_entreprise;

        return $this;
    }

    public function getAdressePostale(): ?string
    {
        return $this->adresse_postale;
    }

    public function setAdressePostale(?string $adresse_postale): self
    {
        $this->adresse_postale = $adresse_postale;

        return $this;
    }

    public function getAdresseGeographique(): ?string
    {
        return $this->adresse_geographique;
    }

    public function setAdresseGeographique(?string $adresse_geographique): self
    {
        $this->adresse_geographique = $adresse_geographique;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
