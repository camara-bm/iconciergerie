<?php

namespace App\Controller;


use App\Repository\DriverRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Route("/")
 */
class DriversController extends AbstractController
{
    /**
     * @Route("/drivers", name="pages.drivers", methods={"GET"})
     * @param DriverRepository $driverRepository
     * @return Response
     */
    public function drivers(DriverRepository $driverRepository):Response{
        return $this->render("pages/drivers.html.twig",[
            'drivers' => $driverRepository->findAll(),
        ]);
    }
}